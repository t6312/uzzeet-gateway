package service

import (
	"gitlab.com/t6312/uzzeet-gateway/controller/resolver"
	"gitlab.com/t6312/uzzeet-gateway/controller/resolver/manual"
	"gitlab.com/t6312/uzzeet-gateway/libs"
	"gitlab.com/t6312/uzzeet-gateway/libs/helper"
	"gitlab.com/t6312/uzzeet-gateway/libs/helper/serror"
)

func NewResolver() (resolv resolver.Resolver, errx serror.SError) {
	switch helper.Env(libs.AppCluster, libs.ClusterLocal) {

	default:
		resolv, errx = manual.NewManualResolver()
	}

	if errx != nil {
		return resolv, errx
	}

	errx = resolv.Register()
	if errx != nil {
		return resolv, errx
	}

	return resolv, errx
}
