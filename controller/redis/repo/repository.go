package repo

import "gitlab.com/t6312/uzzeet-gateway/models"

type CompositeRepository interface {
	GetRedisByID(models.CompositeID) (models.Composite, error)
}
